const express = require('express');
const app = express();
const cors = require('cors');
const Mongo = require('mongodb').MongoClient;
const { json } = require('express');
const { Pool, Client } = require('pg');
const Conexion = require('./connection');



app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());


// const pool = new Pool({
//   user: 'postgres',
//   host: 'localhost',
//   database: 'NoteApp',
//   password: '1234',
//   port: 5432,
// })


// const client = new Client({
//   user: 'postgres',
//   host: 'localhost',
//   database: 'NoteApp',
//   password: '1234',
//   port: 5432,
// })

// client.connect();


const pool = Conexion.pool;
const client = Conexion.client;

client.connect();

let id = {};

app.get("/", async (req, res) => {


  // const resx = await client.query('SELECT "ID", "User", "Password" FROM public."Users";');
  const resx = await client.query(`INSERT INTO public."Users"("User", "Password") VALUES ('${req.query.Nombre}', '${req.query.Password}');`);

  pool.end();
  client.end();

  res.send(resx.rows);

});

// app.get("/getid" ,(req, res) => {

//   return res.send(id);

// })

// app.post("/", async (req, res) => {


//   const resx = await client.query(`SELECT "Usuario", "Password" FROM public."Users"
//   WHERE "Usuario"='${req.body.User}' AND "Password"='${req.body.Password}';`);
//   // const resx = await client.query(`INSERT INTO public."Users"("User", "Password") VALUES ('${req.query.Nombre}', '${req.query.Password}');`); 


//   if (resx.rows.length === 0) {
//     return res.send(false);
//   }


//   // pool.end();
//   // client.end();

//   let result = resx.rows[0];

//   if (result.Usuario === req.body.User && result.Password === req.body.Password)
//     return res.send(true);
//   else {
//     return res.send(false);
//   }

// });

app.post("/", async (req, res) => {


  const resx = await client.query(`SELECT "ID", "Usuario", "Password" FROM public."Users"
  WHERE "Usuario"='${req.body.User}' AND "Password"='${req.body.Password}';`);
  // const resx = await client.query(`INSERT INTO public."Users"("User", "Password") VALUES ('${req.query.Nombre}', '${req.query.Password}');`); 


  if (resx.rows.length === 0) {
    return res.send(false);
  }


  // pool.end();
  // client.end();

  let result = resx.rows[0];


  if (result.Usuario === req.body.User && result.Password === req.body.Password) {

    let json = {
      "data": true,
      "id": result.ID
    }

    id = json;

    return res.send(json);

  }
  else {
    return res.send(false);
  }

});

app.post('/Registro', (req, res) => {


  if (req.body.Password === req.body.PasswordC) {

    client.query(`INSERT INTO public."Users"("Nombre", "Apellido", "Usuario", "Password")
    VALUES ('${req.body.Name}', '${req.body.LastName}', '${req.body.User}', '${req.body.Password}');`);
  } else {

    return res.send(false);
  }


  return res.send(true);


});



app.post('/Notes', (req, res) => {


  let title = req.body.title, content = req.body.content;

  console.log(title);
  console.log(content);


  if (req.body.title.length != 0 && req.body.content.length != 0) {

    client.query(`INSERT INTO public."Notes"("Titulo", "Contenido", "Lugar", "IdUsuario") VALUES ( '${req.body.title}', '${req.body.content}', '${req.body.place}', '${req.body.idUser}');`);

    res.send(true);

  } else {

    return res.send(false);
  }

});


// app.get('/Notes', async (req, res) => {

//   let queryResult = await client.query(`SELECT PUBLIC."Notes"."ID", "Titulo", "Contenido" FROM PUBLIC."Notes" 
//   INNER JOIN  PUBLIC."Users" ON public."Notes"."IdUsuario" = public."Users"."ID";`)
 
//     return res.send(queryResult);
    
// });


app.get('/Notes', async (req, res) => {

  let queryResult = await client.query(`SELECT PUBLIC."Notes"."ID", "Titulo", "Contenido" FROM PUBLIC."Notes" 
  INNER JOIN  PUBLIC."Users" ON public."Notes"."IdUsuario" = public."Users"."ID"
  AND "Lugar" = 0;`)

  // console.log(req.query.id);
  // let queryResult = await client.query(`SELECT "Titulo", "Contenido" FROM public."Notes" WHERE "IdUsuario"='${req.query.id}' and "Lugar"=0`);
  // console.log(req.query.id);

    return res.send(queryResult);
    
});



app.put("/Notes/:id", async (req, res) => {
 
   client.query(`UPDATE public."Notes" SET "Titulo"='${req.body.title}', "Contenido"='${req.body.content}' WHERE "ID"=${req.body.id};`);


   res.send(true);
   
  
})


// app.put("/Notes/:id/:place", async (req, res) => {
 

//   client.query(`UPDATE public."Notes" SET "Lugar"='${req.body.place}', "IdUsuario"='${req.body.idUser}' WHERE "ID"='${req.body.id}';`);


//   res.send(console.log(holaaaaaaaa));
  
 
// })


app.put("/Notes", async (req, res) => {
 
   client.query(`UPDATE public."Notes" SET "Lugar"='${req.body.place}', "IdUsuario"='${req.body.idUser}' WHERE "ID"='${req.body.id}';`);


  res.send(true);
  
 
})


app.get('/Favorites', async (req, res) => {

  let queryResult = await client.query(`SELECT PUBLIC."Notes"."ID", "Titulo", "Contenido" FROM PUBLIC."Notes" 
  INNER JOIN  PUBLIC."Users" ON public."Notes"."IdUsuario" = public."Users"."ID"
  AND "Lugar" = 1;`)

  // let queryResult = await client.query(`SELECT "Titulo", "Contenido" FROM public."Notes" WHERE "IdUsuario"='${req.query.id}' and "Lugar"=1`);
 
    return res.send(queryResult);
    
});


app.put("/Favorites/:id", async (req, res) => {
 
  client.query(`UPDATE public."Notes" SET "Titulo"='${req.body.title}', "Contenido"='${req.body.content}' WHERE "ID"=${req.body.id};`);


  res.send(true);
  
 
})

app.put("/Favorites", async (req, res) => {
 
  client.query(`UPDATE public."Notes" SET "Lugar"='${req.body.place}', "IdUsuario"='${req.body.idUser}' WHERE "ID"='${req.body.id}';`);


 res.send(true);
 

});

app.get('/Trash', async (req, res) => {

  let queryResult = await client.query(`SELECT PUBLIC."Notes"."ID", "Titulo", "Contenido" FROM PUBLIC."Notes" 
  INNER JOIN  PUBLIC."Users" ON public."Notes"."IdUsuario" = public."Users"."ID"
  AND "Lugar" = 2;`)
 
    return res.send(queryResult);
    
});

app.put("/Trash/:id", (req, res) => {
 
  client.query(`UPDATE public."Notes" SET "Titulo"='${req.body.title}', "Contenido"='${req.body.content}' WHERE "ID"=${req.body.id};`);


  res.send(true);
  
 
});


app.put("/Trash", (req, res) => {
 
  client.query(`UPDATE public."Notes" SET "Lugar"='${req.body.place}', "IdUsuario"='${req.body.idUser}' WHERE "ID"='${req.body.id}';`);


 res.send(true);
 

});

app.delete("/Trash", (req, res) => {
 
  client.query(`DELETE FROM public."Notes" WHERE "ID"='${req.body.id}';`);


  res.send(true);
 

});








app.listen(4000, () => {

  console.log("Servidor corriendo");

})


